include(focal)

RUN apt install --yes \
        wget curl iproute2 iptables bridge-utils netmask \
        gnupg ca-certificates openssl

RUN apt install --yes \
        openvpn easy-rsa

RUN curl -O https://raw.githubusercontent.com/angristan/openvpn-install/master/openvpn-install.sh && \
    chmod +x openvpn-install.sh && \
    mv openvpn-install.sh /usr/local/bin/

RUN mkdir -p /dev/net && \
    mknod /dev/net/tun c 10 200 && \
    chmod 0666 /dev/net/tun && \
    AUTO_INSTALL=y /usr/local/bin/openvpn-install.sh

### install onionshare
RUN apt install --yes software-properties-common && \
    add-apt-repository ppa:micahflee/ppa && \
    apt install -y onionshare

### install openvpn-monitor
RUN apt install --yes \
        apache2 libapache2-mod-wsgi python3-geoip2 python3-ipaddr \
        python3-humanize python3-bottle python3-semantic-version \
        geoip-database geoipupdate git
RUN git clone https://github.com/furlongm/openvpn-monitor.git /var/www/html/monitor && \
    cp /var/www/html/monitor/openvpn-monitor.conf.example /var/www/html/monitor/openvpn-monitor.conf && \
    chown -R www-data: /var/www/html/monitor/ && \
    ln -s /usr/bin/python3 /usr/bin/python
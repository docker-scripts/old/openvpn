cmd_create_help() {
    cat <<_EOF
    create
        Create the container '$CONTAINER'.

_EOF
}

rename_function cmd_create orig_cmd_create
cmd_create() {
    mkdir -p config logs data/published
    orig_cmd_create \
        --cap-add NET_ADMIN \
        --publish ${OVPN_PORT:-1194}:${OVPN_PORT:-1194}/udp \
        --mount type=bind,src="$(pwd)/config",dst=/etc/openvpn \
        --mount type=bind,src="$(pwd)/logs",dst=/var/log/openvpn \
        --mount type=bind,src="$(pwd)/data/published",dst=/var/www/html/clients \
        "$@"    # accept additional options, e.g.: -p 2201:22
}

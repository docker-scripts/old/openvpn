cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    cmd_start && sleep 5

    ds inject ubuntu-fixes.sh
    ds inject ssmtp.sh
    ds inject logwatch.sh $(hostname)
    ds inject apache2-redirect-to-https.sh

    ds inject install-openvpn.sh
    ds client revoke "client" > /dev/null

    if [[ -n $DOMAIN ]]; then
        _check_config
        ds inject monitor.sh   # install openvpn-monitor
        ds inject publish.sh
    fi
}

_check_config() {
    [[ $ADMIN == 'admin' ]] &&\
        fail "Error: ADMIN on 'settings.sh' should be changed for security reasons."

    [[ $PASS == 'pass123' ]] &&\
        fail "Error: PASS on 'settings.sh' should be changed for security reasons."
}

#!/bin/bash

### create an apache2 config
cat <<EOF >> /etc/apache2/conf-available/publish-clients.conf
<Directory /var/www/html/clients>
    Options -Indexes
</Directory>
EOF

### enable the config and restart apache2
mkdir -p /var/www/html/clients
a2enconf publish-clients
systemctl restart apache2

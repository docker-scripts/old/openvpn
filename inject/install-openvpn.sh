#!/bin/bash -x

source /host/settings.sh

main() {
    install

    if [[ -z $BRIDGE_MODE ]]; then
        config_server
    else
        config_server_bridge
    fi

    update_any_existing_client_configs
    systemctl restart openvpn@server
}

install() {
    setup_tun_dev
    AUTO_INSTALL=y PORT_CHOICE=2 PORT=$OVPN_PORT openvpn-install.sh # > /dev/null

    # comment a server setting that breaks the DNS on iPhone
    # see: https://community.openvpn.net/openvpn/ticket/1007
    sed -i /etc/openvpn/server.conf \
        -e '/dhcp-option DNS 127.0.0.11/ s/^/# /'
}

setup_tun_dev() {
    cat <<EOF > /usr/local/bin/setup-tun-dev.sh
#!/bin/bash -x

if [[ ! -c /dev/net/tun ]]; then
    rm -f /dev/net/tun
    mkdir -p /dev/net
    mknod /dev/net/tun c 10 200
    chmod 0666 /dev/net/tun
fi
EOF
    chmod +x /usr/local/bin/setup-tun-dev.sh

    cat <<EOF > /etc/systemd/system/setup-tun-dev.service
[Unit]
Description=Create a tun device
After=network-online.target
Wants=network-online.target

[Service]
ExecStart=/usr/local/bin/setup-tun-dev.sh

[Install]
WantedBy=multi-user.target
EOF

    systemctl enable setup-tun-dev.service
    systemctl start setup-tun-dev.service
    sleep 5
}

config_server() {
    # setup the network
    sed -i /etc/openvpn/server.conf \
        -e "/^server/ c server $OVPN_NETWORK $OVPN_BROADCAST 'nopool'"

    # setup the IP pool
    local from=$(echo $OVPN_IP_POOL | cut -d: -f1)
    local to=$(echo $OVPN_IP_POOL | cut -d: -f2)
    local from_ip=${OVPN_NETWORK%.*}.$from
    local to_ip=${OVPN_NETWORK%.*}.$to
    sed -i /etc/openvpn/server.conf \
        -e "/^server/ a ifconfig-pool $from_ip $to_ip"
}

config_server_bridge() {
    setup_bridge_dev
    setup_firewall
    
    # replace 'dev tun' with 'dev tap0' on server config
    sed -i /etc/openvpn/server.conf \
        -e 's/^dev tun/dev tap0/'

    # comment out current server configuration
    sed -i /etc/openvpn/server.conf \
        -e '/^server / s/^/# /' \
        -e '/^ifconfig-pool / s/^/# /'

    # setup server-bridge configuration
    local ip_addr=$(ip addr show br0 | grep inet | sed 's/ *inet //' | cut -d' ' -f1)
    local network=$(netmask $ip_addr -s | tr -d ' ' | cut -d'/' -f1)
    local netmask=$(netmask $ip_addr -s | tr -d ' ' | cut -d'/' -f2)
    local from=$(echo $OVPN_IP_POOL | cut -d: -f1)
    local to=$(echo $OVPN_IP_POOL | cut -d: -f2)
    local from_ip=${network%.*}.$from
    local to_ip=${network%.*}.$to
    sed -i /etc/openvpn/server.conf \
        -e "/ifconfig-pool/ a server-bridge $network $netmask $from_ip $to_ip"

    # enable client-to-client connection
    if [[ -n $CLIENT_TO_CLIENT ]]; then
        sed -i /etc/openvpn/server.conf \
            -e '/server-bridge/ a client-to-client'
    fi

    # replace 'dev tun' with 'dev tap' on client template config
    sed -i /etc/openvpn/client-template.txt \
        -e 's/^dev tun/dev tap/'
}

setup_bridge_dev() {
    cat <<'EOF' > /usr/local/bin/setup-bridge-dev.sh
#!/bin/bash -x

ip addr show br0 &>/dev/null && exit 0

# get current IP address and gateway
ip_addr=$(ip addr show eth0 | grep inet | sed 's/ *inet //' | cut -d' ' -f1)
gateway=$(ip route | grep default | cut -d' ' -f3)

# make a TAP interface and create the bridge
openvpn --mktun --dev tap0
brctl addbr br0
brctl addif br0 eth0
brctl addif br0 tap0

ip link set dev br0 up
ip link set dev tap0 up
ip addr del $ip_addr dev eth0
ip addr add $ip_addr dev br0
ip route add to default via $gateway dev br0
EOF
    chmod +x /usr/local/bin/setup-bridge-dev.sh
    /usr/local/bin/setup-bridge-dev.sh

    cat <<EOF > /etc/systemd/system/setup-bridge-dev.service
[Unit]
Description=Create a tap device
After=network-online.target
Wants=network-online.target

[Service]
ExecStart=/usr/local/bin/setup-bridge-dev.sh

[Install]
WantedBy=multi-user.target
EOF

    systemctl enable setup-bridge-dev.service
    systemctl start setup-bridge-dev.service
    sleep 5
}

setup_firewall() {
    # change firewall rules
    /etc/iptables/rm-openvpn-rules.sh
    cat <<EOF > /etc/iptables/add-openvpn-rules.sh
#!/bin/bash -x
iptables -I INPUT 1 -i tap0 -j ACCEPT
iptables -I INPUT 1 -i br0 -j ACCEPT
iptables -I FORWARD 1 -i br0 -j ACCEPT
# iptables -t nat -I POSTROUTING 1 -o br0 -j MASQUERADE
# iptables -I INPUT 1 -i br0 -p udp --dport $OVPN_PORT -j ACCEPT
EOF
    /etc/iptables/add-openvpn-rules.sh

    cat <<EOF > /etc/iptables/rm-openvpn-rules.sh
#!/bin/bash -x
iptables -D INPUT -i tap0 -j ACCEPT
iptables -D INPUT -i br0 -j ACCEPT
iptables -D FORWARD -i br0 -j ACCEPT
# iptables -t nat -D POSTROUTING -o br0 -j MASQUERADE
# iptables -D INPUT -i br0 -p udp --dport $OVPN_PORT -j ACCEPT
EOF
}

update_any_existing_client_configs() {
    local dev_type='tun'
    [[ -n $BRIDGE_MODE ]] && dev_type='tap'
    local client_configs=$(ls /host/data/clients/*.ovpn 2>/dev/null)
    if [[ -n $client_configs ]]; then
        for config_file in $client_configs; do
            sed -i $config_file \
                -e "/^dev /c dev $dev_type/"
        done
    fi
}

# start the main function
main "$@"

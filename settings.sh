APP=openvpn

### OpenVPN settings
OVPN_PORT="1194"

OVPN_NETWORK="10.8.0.0"
OVPN_NETMASK="255.255.255.0"

### The range of IPs that will be assigned to the clients.
### If assigning a fixed IP to a client, use an IP outside of this range.
OVPN_IP_POOL="100:250"

#######################################################################

### If the Bridge Mode is enabled (uncommented), then the clients will
### be connected to the same network as the docker container (which is
### usually an internal docker network). This means that the OVPN_NETWORK
### and OVPN_NETMASK settings above will be ignored. However OVPN_IP_POOL
### will still be used.
### If CLIENT_TO_CLIENT is enabled as well, then the clients will also
### be able to see (ping) each-other.
#BRIDGE_MODE=yes
#CLIENT_TO_CLIENT=yes

#######################################################################

### Uncomment DOMAIN to enable openvpn-monitor
#DOMAIN=ovpn.example.org

ADMIN=admin
PASS=pass123
